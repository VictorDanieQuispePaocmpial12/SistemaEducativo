﻿using SistemaEducativo.Entidades;

namespace SistemaEducativo.Negocio.Interfaces
{
    public interface IPersonaService
    {
        Persona ObtenerPersonaPorDni(string dni);
        IEnumerable<Persona> ObtenerTodasLasPersonas();
        void InsertarPersona(Persona persona);
        void ActualizarPersona(Persona persona);
        void EliminarPersonaPorDni(string dni);
    }
}
