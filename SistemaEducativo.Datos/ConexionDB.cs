﻿using System;
using System.Data.SqlClient;
using SistemaEducativo.Datos.Interfaces;

namespace SistemaEducativo.Datos
{
    public class ConexionDB : IConexionDB
    {
        private readonly string Base = "menu_dinamico";
        private readonly string Servidor = "DESKTOP-8LA7DUM";
        private readonly string Usuario = "sa";
        private readonly string Clave = "admin";
        private readonly bool Seguridad = true;

        // Inicializa instance aquí
        private static ConexionDB instance = new ConexionDB();

        private ConexionDB()
        {
        }

        public static IConexionDB GetInstance()
        {
            return instance;
        }

        public SqlConnection CrearConexion()
        {
            try
            {
                var connectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = Servidor,
                    InitialCatalog = Base,
                    IntegratedSecurity = Seguridad
                };

                if (!Seguridad)
                {
                    connectionStringBuilder.UserID = Usuario;
                    connectionStringBuilder.Password = Clave;
                }

                return new SqlConnection(connectionStringBuilder.ConnectionString);
            }
            catch (SqlException ex)
            {
                // Aquí podrías manejar las excepciones específicas de SQL Server
                throw new Exception("Error al crear la conexión a la base de datos.", ex);
            }
            catch (Exception ex)
            {
                // Otras excepciones generales
                throw new Exception("Error general al crear la conexión a la base de datos.", ex);
            }
        }
    }
}
