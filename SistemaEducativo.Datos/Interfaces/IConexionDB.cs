﻿using System.Data.SqlClient;

namespace SistemaEducativo.Datos.Interfaces
{
    public interface IConexionDB
    {
        SqlConnection CrearConexion();
    }
}