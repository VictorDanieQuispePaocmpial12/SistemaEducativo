﻿namespace SistemaEducativo.Entidades
{
    public class Persona
    {
        public string Dni { get; set; }
        public bool Estado { get; set; }
        public string Ruc { get; set; }
        public string ApellidosPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Nombres { get; set; }
        public int Edad { get; set; }
        public char Sexo { get; set; }
        public string Foto { get; set; }
        public string Email { get; set; }
    }
}
